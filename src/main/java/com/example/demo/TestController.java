package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class TestController {

	@Autowired
	private TestRepository repo;
	
	@GetMapping("/status")
	public String status() {
		return "{\"status\": \"OK!\"}";
	}
	
	@GetMapping("/new")
	@ResponseStatus(value=HttpStatus.CREATED)
	public void novo() {
		repo.save(new Test("funcionou"));
	}
	
	@GetMapping("/{id}")
	public Test find(@PathVariable Long id) {
		return repo.findById(id).get();
	}
}
